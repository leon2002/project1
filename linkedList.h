#ifndef LINKLIST_H
#define LINKLIST_H


/* a linked list contains positive integer values. */
typedef struct list
{
	unsigned int value;
	list* next;

} list;

//push - insert element to the top of the stack
void push(list** s, int element);

//pop - remove element from the top of the stack
void pop(list** s);


#endif /* LINKLIST_H */