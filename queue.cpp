#include "queue.h"
#include <iostream>

void initQueue(queue* q, unsigned int size){
	q->maxSize = size;
	q->queueArr = new unsigned int[size];
	q->amount = 0;

}


void cleanQueue(queue* q) {
	delete[] q->queueArr;


}


void enqueue(queue* q, unsigned int newValue) {
	q->queueArr[q->amount] = newValue;
	q->amount++;


}

int dequeue(queue* q) {
	int i = 0, ans = 0;
	if (q->amount == 0) {
		ans = -1;
	}
	else {
		ans = q->queueArr[0]; // top of queue
		for (int i = 1; i < q->amount; i++) {
			q->queueArr[i - 1] = q->queueArr[i];

		}
		q->queueArr[q->amount] = NULL;
		q->amount--;
	}
	return ans;

}
