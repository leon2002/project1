#include "stack.h"
#include "linkedList.h"
#include <iostream>

void push(stack* s, unsigned int element) {
	list* tmp = s->head;
	while (tmp->next != NULL) {
		tmp = tmp->next;
	}
	tmp->next = new list;
	tmp->next->value = element;
	tmp->next->next = NULL;


}

int pop(stack* s) {

	list* tmp = s->head;
	int ans = 0;
	if (tmp == NULL) {
		ans = -1;
	}
	else {
		while (tmp->next->next != NULL && tmp->next != NULL) {
			tmp = tmp->next;
		}
		ans = tmp->next->value;
		delete tmp->next;
		tmp->next = NULL;
		return ans;
	
	}

}

void initStack(stack* s) {
	s->head = new list;
	s->head->next = NULL;

}
void cleanStack(stack* s) {
	delete s->head;

}



